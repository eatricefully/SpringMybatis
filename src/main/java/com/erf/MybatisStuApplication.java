package com.erf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @description MybatisStuApplication
 * @author zhangzy
 * @date 2022/5/15 16:44
 */
@SpringBootApplication
public class MybatisStuApplication {

    public static void main(String[] args) {
        SpringApplication.run(MybatisStuApplication.class, args);
    }

}

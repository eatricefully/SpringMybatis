package com.erf.controller;

import com.erf.entity.UserInfo;
import com.erf.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @description UserInfoController
 * @author zhangzy
 * @date 2022/5/15 16:44
 */
@RestController
public class UserInfoController {
    @Autowired
    UserInfoService userInfoService;

    @RequestMapping("insert")
    public int insert(@RequestParam String name, @RequestParam Integer age) {
        UserInfo userInfo = UserInfo.builder().name(name).age(age).build();
        return userInfoService.insert(userInfo);
    }

    @RequestMapping("list")
    @ResponseBody
    public List<UserInfo> getAllUserInfo() {
        return userInfoService.listAll();
    }

}

package com.erf.entity;

import lombok.Builder;
import lombok.Data;

/**
 * @description UserInfo
 * @author zhangzy
 * @date 2022/5/15 16:44
 */
@Data
@Builder
public class UserInfo {
    private int id;
    private String name;
    private int age;
}

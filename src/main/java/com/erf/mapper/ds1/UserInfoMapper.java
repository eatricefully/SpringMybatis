package com.erf.mapper.ds1;

import com.erf.entity.UserInfo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description UserInfoMapper
 * @author zhangzy
 * @date 2022/5/15 16:44
 */
@Repository("userMapper1")
public interface UserInfoMapper {

    @Insert("insert into user_info set name=#{name}, age = #{age}")
    int insert(UserInfo userInfo);

    @Select("select id,name,age from user_info")
    List<UserInfo> list();
}

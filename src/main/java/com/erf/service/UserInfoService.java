package com.erf.service;

import com.erf.entity.UserInfo;
import com.erf.mapper.ds1.UserInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @description UserInfoService
 * @author zhangzy
 * @date 2022/5/15 16:44
 */
@Service
public class UserInfoService {
    @Autowired
    @Qualifier("userMapper1")
    private com.erf.mapper.ds1.UserInfoMapper userInfoMapper1;
    @Autowired
    @Qualifier("userMapper2")
    private com.erf.mapper.ds2.UserInfoMapper userInfoMapper2;

    public int insert(UserInfo userInfo) {
        return userInfoMapper2.insert(userInfo);
    }

    public List<UserInfo> listAll() {
        return userInfoMapper1.list();
    }

}
